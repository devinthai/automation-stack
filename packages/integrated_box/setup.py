import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="integratedbox",
    version="0.0.1",
    author="Devin Thai",
    author_email="dthai@greenlots.com",
    description="A package implementing the boxsdk for the sole purpose of manipulating the bgis-dispatch folder in Box.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/devinthai/automation-stack/src/master/packages/integrated-box/",
    project_urls={
        "Bug Tracker": "",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)