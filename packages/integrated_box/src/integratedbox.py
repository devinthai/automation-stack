from boxsdk import JWTAuth, Client


class Box:
    def __init__(self, box_config_path):
        """
        Create a bgis-folder box object.
        :param box_config_path: Path to box configuration file with authentication information.
        """
        self.bgis_folder = "129681588280"
        self.archive_folder = "129687781308"
        self.box_config = box_config_path
        self.box = self.box_auth()

    def box_auth(self):
        """
        Authenticate with Box using the given box configuration file.
        :return: Box Client object
        """
        try:
            config = JWTAuth.from_settings_file(self.box_config)
        except AttributeError:
            print("Invalid box configuration file on path: " + str(self.box_config))
            return
        except FileNotFoundError:
            print("Box configuration file: {} does not exist.".format(str(self.box_config)))
            return
        client = Client(config)

        return client

    def download_files(self, file_list):
        """
        Download a list of files.
        :param file_list: list of file_ids that need to be downloaded
        :return: files: list of file paths that the files have been downloaded to
        """
        files = []
        for file in file_list:
            path = self.box.file(file).get().name
            with open(path, 'wb') as f:
                self.box.file(file).download_to(f)
            files.append(path)

        return files

    def archive_files(self, file_list):
        """
        Archive a list of files.
        :param
            file_list: list of file_ids that need to be sent to the 'Archive' folder
        :return:
            moved_file: list of file_ids that have been sent to the 'Archive' folder
        """
        moved_files = []
        for file_id in file_list:
            file = self.box.file(file_id)
            folder = self.box.folder(self.archive_folder)

            moved_file = file.move(folder)
            moved_files.append(moved_file)

        return moved_files

    def list_root(self):
        """
        List out the files in the root directory of the bgis dispatch folder.
        :return: list of files_ids
        """
        item_list = []
        folder = self.box.folder(self.bgis_folder)
        for item in folder.get_items():
            if item.type == 'file':
                item_list.append(item.id)

        return item_list

    def list_archive(self):
        """
        List out the files in the 'Archive' folder of the bgis dispatch folder.
        :return:
        """
        item_list = []
        folder = self.box.folder(self.archive_folder)
        for item in folder.get_items():
            if item.type == 'file':
                item_list.append(item.id)

        return item_list
