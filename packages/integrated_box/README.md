# integrated_box
This is a tool that integrates the boxsdk for the sole purpose of interacting with the BGIS-dispatch folder in Box.

## Requirements
This package requires the following:
* boxsdk

# Installation via PIP
```
python3 -m pip install -e ~/automation-stack/packages/integrated-box
```

# Usage Notes
When working with this package, you will be required to specify the path to a box configuration file from the box
developer console.

```
from integrated_box import Box
box = Box('path/to/config/file')
```