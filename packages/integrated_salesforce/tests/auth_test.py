import sys
sys.path.append("..")
from src.integratedsf import Salesforce

def main():
    sf_config = '/Users/devinthai/advanced-services/automation/bgis-integration/.secret/sf_config.json'
    dispatch_job_config = '/Users/devinthai/advanced-services/automation/bgis-integration/.secret/create_dispatch_job.json'
    #sf_config = 'potato'

    sf = Salesforce()
    status, desc = sf.auth(sf_config)
    if status == 200:
        print(sf.access_token)
    else:
        print(status)
        print(desc)
    res = sf.create_job(dispatch_job_config)
    print(res)

    return

main()