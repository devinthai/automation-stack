# import packages

import requests
import io
import json


class Salesforce:
    def __init__(self):
        #self.sf_instance = {{ sf_instance.value }}
        self.sf_instance = 'https://greenlots.my.salesforce.com/'
        #self.login_url = {{ sf_login_url.value }}
        self.login_url = 'https://greenlots.my.salesforce.com/services/oauth2/token'
        #self.create_job_url = {{ sf_create_job_url.value }}
        self.create_job_url = "https://greenlots.my.salesforce.com/services/data/v41.0/jobs/ingest/"
        self.access_token = ''

    def auth(self, file_path):
        """
        Authenticate with Salesforce.
        :param file_path: path to the salesforce configuration file
        :return: response
        """

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        try:
            with open(file_path) as json_file:
                data = json.load(json_file)
        except FileNotFoundError:
            print('{} does not exist.'.format(file_path))
            return '', 'Please specify correct path to sf config.'

        response = requests.post(self.login_url, headers=headers, data=data)
        if response.status_code == 200:
            self.access_token = response.json()['access_token']
        return response.status_code, response.text

    def create_job(self, file_path):
        """
        Create a job for Bulk API 2.0. Sends a POST request to the bulk api 2.0 ingestion point
        :param file_path: path to the job creation configuration file.
        :return: contentUrl: url to be appended to salesforce instance url to upload data for the job
        """
        headers = {
            'Authorization': 'Bearer ' + self.access_token,
            'Content-Type': 'application/json'
        }

        try:
            with open(file_path) as json_file:
                data = json.load(json_file)
        except FileNotFoundError:
            print('{} does not exist.'.format(file_path))
            return '', 'Please specify correct path to job creation config.'

        response = requests.post(self.create_job_url, headers=headers, json=data)
        if response.status_code == 200:
            return response.json()['contentUrl']
        else:
            return '{}: {}'.format(response.json()['errorCode'], response.json()['message'])

    def upload_data(self, content_url, file_path):

        return
