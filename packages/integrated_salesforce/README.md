# integratedsf
This is a tool that integrates the salesforce api for the sole purpose of interacting with the Greenlots Salesforce environment.

## Requirements
This package requires the following:

* pandas
* ratelimit
* requests

# Installation via PIP
```

```

# Usage Notes
When working with this package, you will be required to specify the path to a salesforce configuration file to
authenticate.

```
from integrated_box import Box
box = Box('path/to/config/file')
```