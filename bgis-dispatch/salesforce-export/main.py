import sf_export
from xml.etree import ElementTree as ET
import pandas as pd
import io


def main():
    login_path = '.secret/login.txt'
    create_job_path = 'create-job.xml'
    query_path = 'query.txt'

    sf = sf_export.SfExport()
    auth_response = sf.sf_auth(login_path)
    auth_tree = ET.fromstring(auth_response.text)
    session_id = auth_tree[0][0][0][4].text
    job_response = sf.create_job(create_job_path, session_id)
    job_tree = ET.fromstring(job_response.text)
    job_id = job_tree[0].text
    query_response = sf.execute_bulk_query(query_path, session_id, job_id)
    query_tree = ET.fromstring(query_response.text)
    batch_id = query_tree[0].text
    status = ''
    while status != 'Completed':
        status_response = sf.batch_status(session_id, job_id)
        status_tree = ET.fromstring(status_response.text)
        status = status_tree[0][2].text
    results_response = sf.retrieve_results(session_id, job_id, batch_id)
    csv_output = results_response.text

    field_dict = {
        'GL_Asset_Id__c': 'Station ID',
        'ChargeBox_Id_EVSE__c': 'Chargebox ID',
        'Name': 'Station Name',
        'Manufacturer__c': 'Manufacturer',
        'Model_Number__c': 'Model Name',
        'LocationGL__r.Active_Maintenance_Agreement__r.Package_Type__c': 'O&M Package Type',
        'LocationGL__r.Active_Maintenance_Agreement__r.O_M_Response_Time__c': 'O&M Response Time',
        'LocationGL__r.Active_Maintenance_Agreement__r.Preventative_Maintenance_Visit_Year__c': 'Preventative Maintenance Visits',
        'LocationGL__r.Name': 'Location Name',
        'Enterprise__r.Name': 'Enterprise Name',
        'Network__r.Name': 'Network Name',
        'LocationGL__r.Address1__c': 'Address1',
        'LocationGL__r.Address2__c': 'Address2',
        'LocationGL__r.City__c': 'City',
        'LocationGL__r.State__c': 'State',
        'LocationGL__r.Country__c': 'Country',
        'LocationGL__r.Postal_Code__c': 'Postal Code',
        'LocationGL__r.Site_Host_Contact_Name__c': 'Contact Name',
        'LocationGL__r.Site_Host_Contact_Phone__c': 'Contact Phone',
        'LocationGL__r.Site_Host_Contact_Email__c': 'Contact Email',
        'Firmware_Version__c': 'Firmware Version',
        'IP__c': 'IP Address',
        'Sim__c': 'SIM Card Number',
        'LocationGL__r.Latitude__c': 'Latitude',
        'LocationGL__r.Longitude__c': 'Longitude',
        'LocationGL__r.Ext_ID__c': 'Site ID'
    }

    df = pd.read_csv(io.StringIO(csv_output), sep=',')
    df = df.rename(columns=field_dict)
    df.to_csv('station_export.csv', index=False)
    sf.close_job(session_id, job_id)
    return


main()

