# import packages

import requests
import pandas as pd
import io
from xml.etree import ElementTree
from ratelimit import limits


class SfExport:
    def __init__(self):
        self.sf_instance = 'greenlots.my'
        self.login_url = 'https://login.salesforce.com/services/Soap/u/51.0'
        self.create_job_url = 'https://{}.salesforce.com/services/async/51.0/job'.format(self.sf_instance)

    def sf_auth(self, file_path):
        """
        input:
            file_path: string containing the filepath to the login configuration file
        output:
            response
        """
        headers = {
            'Content-Type': 'text/xml; charset=UTF-8',
            'SOAPAction': 'login'
        }

        data = open(file_path)
        response = requests.post(self.login_url, headers=headers, data=data)
        return response

    def create_job(self, file_path, session_id):
        """
        input:
            file_path: string containing the filepath to the job configuration file
            session_id: Id of the current salesforce session (obtained at salesforce authentication)
        output:
            response
        """
        headers = {
            'X-SFDC-Session': session_id,
            'Content-Type': 'application/xml; charset=UTF-8',
        }

        data = open(file_path)
        response = requests.post(self.create_job_url, headers=headers, data=data)
        return response

    def execute_bulk_query(self, file_path, session_id, job_id):
        """
        input:
            file_path: string containing the filepath to the query file
            session_id: Id of the current salesforce session (obtained at salesforce authentication)
            job_id: Id of the current job to run the query on (obtained at job creation)
        output:
            response
        """
        headers = {
            'X-SFDC-Session': session_id,
            'Content-Type': 'text/csv; charset=UTF-8',
        }

        data = open(file_path)
        request_url = 'https://{}.salesforce.com/services/async/51.0/job/{}/batch'.format(self.sf_instance, job_id)
        response = requests.post(request_url, headers=headers, data=data)
        return response

    @limits(calls=60, period=60)
    def batch_status(self, session_id, job_id):
        """
        input:
            session_id: Id of the current salesforce session (obtained at salesforce authentication)
            job_id: Id of the current job to run the query on (obtained at job creation)
        output:
            response
        """
        headers = {
            'X-SFDC-Session': session_id,
        }

        request_url = 'https://{}.salesforce.com/services/async/51.0/job/{}/batch'.format(self.sf_instance, job_id)
        response = requests.get(request_url, headers=headers)
        return response

    def retrieve_results(self, session_id, job_id, batch_id):
        """
        input:
            session_id: Id of the current salesforce session (obtained at salesforce authentication)
            job_id: Id of the current job to run the query on (obtained at job creation)
            batch_id: Id of the batch (obtained at query execution)
        output:
            response
        """
        headers = {
            'X-SFDC-Session': session_id,
        }

        request_url = 'https://{}.salesforce.com/services/async/51.0/job/{}/batch/{}/result'.format(self.sf_instance,
                                                                                                    job_id, batch_id)
        response = requests.get(request_url, headers=headers)
        response_tree = ElementTree.fromstring(response.text)
        result_id = response_tree[0].text

        headers = {
            'X-SFDC-Session': session_id,
        }

        request_url = 'https://{}.salesforce.com/services/async/51.0/job/{}/batch/{}/result/{}'.format(self.sf_instance,
                                                                                                       job_id, batch_id,
                                                                                                       result_id)

        response = requests.get(request_url, headers=headers)

        return response

    def close_job(self, session_id, job_id):
        """
        input:
            session_id: Id of the current salesforce session (obtained at salesforce authentication)
            job_id: Id of the current job to run the query on (obtained at job creation)
        output:
            response
        """
        headers = {
            'X-SFDC-Session': session_id,
            'Content-Type': 'text/csv; charset=UTF-8',
        }

        data = open('close-job.xml')
        request_url = 'https://{}.salesforce.com/services/async/51.0/job/{}'.format(self.sf_instance, job_id)
        response = requests.post(request_url, headers=headers, data=data)

        return response


