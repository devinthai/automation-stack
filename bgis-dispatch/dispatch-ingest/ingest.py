from integratedbox import Box


def main():
    box_config_path = '../config/box_config.json'
    box = Box(box_config_path)
    new_dispatches = box.list_root()
    downloaded_files = box.download_files(new_dispatches)
    archived_files = box.archive_files(downloaded_files)
